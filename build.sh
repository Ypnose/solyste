#!/bin/sh
# Script by Ypnose - http://ywstd.fr
set -e

SOL_SCRIPT="${0##*/}"

p_err() {
	printf "%s\n" "$1" >&2
	exit 1
}

usage() {
	p_err "usage: ${0##*/} [-c BUILD CONFIG] [-p] [MACHINE]"
}

# Run the specified script ONLY IF not already completed!
run_ifnot() {
	# local isn't specified by POSIX!
	local PRGM_RUN

	PRGM_RUN="${@#*/}"
	PRGM_RUN="$(echo ".solyste_${PRGM_RUN}_done" | tr -s ' ' '_')"
	if [ ! -e "$1" ]; then
		p_err "$SOL_SCRIPT: $1 not found"
	fi
	if [ -e "$PRGM_RUN" ]; then
		printf "$SOL_SCRIPT: \"%s\" already completed\n" "$*"
		return 0
	fi
	sh "$@"
	>"$PRGM_RUN"
}

# Add an option to run an user script when all scripts are finished
while getopts ":c:p" opt; do
	case $opt in
		c)
			if [ ! -r "$OPTARG" ]; then
				p_err "$OPTARG is either missing or unreadable"
			fi
			. "$(readlink -f "$OPTARG")"
			;;
		p)
			SOL_PURGE="YES"
			;;
		*)
			usage
			;;
	esac
done
shift $(( OPTIND - 1 ))

if [ "$#" -ne 1 ]; then
	usage
fi

SOL_BOARD="$1"

if [ "$SOL_PURGE" = "YES" ]; then
	rm -f .solyste_*_done
	sh scripts/prepare-workdir "$SOL_BOARD"
fi
run_ifnot scripts/create-userland "$SOL_BOARD" libs
run_ifnot scripts/create-userland "$SOL_BOARD" base "$SOL_OTHERPKGS"
run_ifnot scripts/build-kernel "$SOL_BOARD"
run_ifnot scripts/setup-system "$SOL_BOARD"
rm .solyste_*_done

exit
