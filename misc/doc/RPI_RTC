Set up Raspberry Pi RTC
=======================

 * Make sure the kernel supports I2C bus (CONFIG_I2C + CONFIG_I2C_BCM2708)
 * Plug the RTC module (here DS3231) and power on the RPi
 * Connect on the RPi and launch 'i2cdetect -y 1'. The chip will be
   listed at ID 68:

 $ i2cdetect -y 1
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- --
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
60: -- -- -- -- -- -- -- -- 68 -- -- -- -- -- -- --
70: -- -- -- -- -- -- -- --

 * Recompile the kernel to make sure it supports the RTC module (here
   CONFIG_RTC_DRV_DS1307)
 * Install the kernel, reboot the system and launch the following
   command:

 $ echo "ds3231 0x68" >/sys/class/i2c-adapter/i2c-1/new_device

 * 'i2cdetect' will replace '68' ID by 'UU'. You should be able to read
   the clock from the RTC:
 
 $ i2cdetect -y 1
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- --
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
60: -- -- -- -- -- -- -- -- UU -- -- -- -- -- -- --
70: -- -- -- -- -- -- -- --
 $ hwclock -r
Wed Jan 11 18:57:51 2017  0.000000 seconds
 $ cat /proc/driver/rtc 
rtc_time        : 18:58:41
rtc_date        : 2017-01-11
alrm_time       : 00:00:00
alrm_date       : 1970-01-01
alarm_IRQ       : no
alrm_pending    : no
update IRQ enabled      : no
periodic IRQ enabled    : no
periodic IRQ frequency  : 1
max user IRQ frequency  : 64
24hr            : yes

 * To automatically enable this device at boot, you must activate the
   'i2c-rtc' overlay. Before doing so, just make sure your kernel
   supports CONFIG_OF_OVERLAY! Edit '/boot/config.txt' and append the
   following line (modify the module accordingly).

 $ vi config.txt
 dtoverlay=i2c-rtc,ds3231

 * After a reboot, the RTC device will be automatically detected. If
   your kernel supports CONFIG_RTC_HCTOSYS, the system time is set from
   the RTC.

 $ dmesg | grep -i rtc
[    0.984102] rtc-ds1307 1-0068: rtc core: registered ds3231 as rtc0
[    1.076644] rtc-ds1307 1-0068: setting system clock to 2017-01-11 19:57:02 UTC (1484164622)
