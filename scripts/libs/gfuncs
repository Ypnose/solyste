# Global functions
# Ypnose - http://ywstd.fr

# This file is sourced by the other scripts. Be careful !

# ALWAYS AFTER libs/gvars!

# Functions
# sol_err [MESSAGE]
sol_err() {
	printf "ERR: %s\n" "$1" >&2
	exit 1
}

# sol_act [MESSAGE]
sol_act() {
	printf "\033[1m=> [${SOL_SCRIPT}] %s\033[0m\n" "$1"
}

chk_instru() {
	if [ -z "$SOL_INSTRU" ]; then
		sol_err "SOL_INSTRU variable is missing!"
	fi
}

print_verbconf() {
	sol_act "The following settings will be used:"
	cat <<-EOF
	BASEDIR: $SOL_WORK_DIR
	PATH:    $PATH
	TARGET:  $SOL_TARGET
	CFLAGS:  $CFLAGS
	EOF
}

# chk_path [COMMAND]
chk_path() {
	# Return binaries ONLY. POSIX 'command' builtin returns functions.
	# local isn't specified by POSIX!
	local IFS
	local p

	IFS=':'
	for p in $PATH; do
		if [ ! -d "$p" ]; then
			continue
		fi
		if [ -x "${p}/${1}" ]; then
			printf "%s\n" "${p}/${1}"
			return 0
		fi
	done
}

# chk_deps [COMMAND1] [COMMAND2] [COMMAND...]
chk_deps() {
	# local isn't specified by POSIX!
	local d
	local DEP

	for d in "$@"; do
		if [ -z "$(chk_path "$d")" ]; then
			DEP="$DEP $d"
		fi
	done
	if [ -n "$DEP" ]; then
		sol_err "The following deps are missing:${DEP}"
	fi
}

chk_core() {
	# nproc is provided by coreutils
	if [ -n "$(chk_path nproc)" ]; then
		MCORE="$(nproc)"
	# In case we are not using coreutils
	elif [ -r "/proc/cpuinfo" ]; then
		MCORE="$(grep -c 'model name' /proc/cpuinfo)"
	# Secure fallback
	else
		MCORE="1"
	fi
}

# get_tarb [TARBALL URL] [TARBALL DEST]
get_tarb() {
	# local isn't specified by POSIX!
	local URL
	local REF

	URL="$1"
	# Needed for 'build-kernel'
	if [ -n "$2" ]; then
		REF="${SOL_FILES_DIR}/${2}"
	else
		REF="${SOL_FILES_DIR}/${URL##*/}"
	fi
	if [ -e "$REF" ]; then
		return 0
	fi
	sol_act "Downloading ${URL##*/}..."
	mkdir -p "$SOL_FILES_DIR"
	if [ -n "$(chk_path curl)" ]; then
		curl -f -L "$URL" -o "$REF"
	elif [ -n "$(chk_path wget)" ]; then
		wget -O "$REF" "$URL"
	fi
}

# chk_sums [FILE] [SHA256 HASH]
chk_sums() {
	# local isn't specified by POSIX!
	local SRC
	local SUM
	local STRIP_SUM

	SRC="${SOL_FILES_DIR}/${1}"
	SUM="$2"
	if [ "$#" -ne 2 ]; then
		sol_err "chk_sums: Wrong number of arguments"
	fi
	sol_act "Verifying ${SRC##*/} sums... "
	STRIP_SUM="$(sha256sum "$SRC")"
	STRIP_SUM="${STRIP_SUM%% *}"
	if [ "$STRIP_SUM" != "$SUM" ]; then
		sol_err "${SRC##*/} checksums are invalid!"
	fi
}

# ext_tarb [TARBALL]
ext_tarb() {
	# local isn't specified by POSIX!
	local ARC

	ARC="${SOL_FILES_DIR}/${1}"
	sol_act "Extracting ${ARC##*/}..."
	mkdir -p "$SOL_EXTRACT_DIR"
	case "$ARC" in
		*.tar.bz2) tar -jxf "$ARC" -C "$SOL_EXTRACT_DIR" ;;
		*.tar.gz)  tar -zxf "$ARC" -C "$SOL_EXTRACT_DIR" ;;
		*.tar.xz)  tar -Jxf "$ARC" -C "$SOL_EXTRACT_DIR" ;;
		*.tgz)     tar -zxf "$ARC" -C "$SOL_EXTRACT_DIR" ;;
		*)       sol_err "$ARC archive is not supported" ;;
	esac
}

# Simple tests for the required tools
# Need a downloader...
if [ -z "$(chk_path curl)" ] && [ -z "$(chk_path wget)" ]; then
	sol_err "Unable to find a downloader (curl or wget)"
fi

# ...and some other things on the host!
chk_deps make

# CPU core(s)
chk_core
