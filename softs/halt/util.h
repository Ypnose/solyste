/* See LICENSE file for copyright and license details. */
#include "arg.h"

/* eprintf.c */
extern char *argv0;

/* eprintf.c */
void enprintf(int, const char *, ...);
void eprintf(const char *, ...);
void weprintf(const char *, ...);
