solyste - [s]illy, [o]dd and [l]ightweight [y]pnose's [st]atic [e]nvironment
============================================================================

**solyste** is a tiny static environment which aims to be stupidly easy
(to use or maintain), minimal and customizable. It will be suitable for
routers, firewalls, gateways, serial client or other embedded network
hardware. If you need a highly reliable system (power outage, filesystem
corruption, data loss, malicious access), **solyste** will do the job.

## Why?

Over the years, I contributed to many projects and sometimes I felt my
contributions were not appreciated sufficiently. I also wanted to build
systems exactly how I imagine them : rock solid, small, autonomous.

First of all, it's good to say I didn't "steal" **anything** from the
existing distros. Everything here is written from scratch. I was
probably inspired by some parts here and there, but I just worked alone
in my little corner. I also decided to try static linking by myself,
despite the negative (and inaccurate) feedback I heard.

## Goals

Here a few ideas:

 * Build a small static userbase : 10-15MiB is the limit (including the
   kernel and without manpages).
 * Kernels without loadable modules.
 * Avoid useless complexity (that means a simple filesystem or
   non-bloated C softwares).
 * Support other architectures: `ARM`, `ARM64`, `MIPS` or `MIPS64`.
 * Read-only filesystem.

The full list is available on the main
[website](https://ywstd.fr/solyste/).

## How to test the current work?

Before launching the script, it's good to know that it's not over. I'm
still developing it. Therefore, bugs may happen. You are warned.

## Build hierarchy

The default build directory is `/build/solyste/`. You must create it
by hand ! If you didn't change the values in `gvars` (located inside
`scripts/libs/`), it is organized like this:

  - `/build/solyste/cross-*/`  → cross compiler
  - `/build/solyste/extract/`  → sources and compilations
  - `/build/solyste/files/`    → software tarballs
  - `/build/solyste/kernel-*/` → kernels (will end by the machine name, like `rpi`)
  - `/build/solyste/libs-*/`   → libraries (will end by the machine name, like `rpi`)
  - `/build/solyste/solfs-*/`  → rootfs (will end by the machine name, like `rpi`)

Example:

	$ ls /build/solyste
	cross-arm/  extract/  files/  kernel-rpi/  libs-rpi/  solfs-rpi/

Instead of modifying `gvars` to change `SOL_WORK_DIR` (the parent dir),
you can export some variable to the environment:

	$ export SOL_WORK_DIR=/tmp/solyste-build

## Prerequisites

To build **solyste**, a valid and working cross compiler is needed. It
must targets the required architecture, like `arm` or any other
architecture matching your hardware.

I recommend you to use
[musl-cross-make](https://github.com/richfelker/musl-cross-make)
which was created by Rich Felker, the main
[musl](https://musl.libc.org/) developper. Read the upstream
documentation to understand how it works. ~~`musl-cross-make` is often
updated by Rich himself and therefore, you can be sure to use the latest
`musl` release.~~ (last update was in 2022).

To help you in this task, I include samples in `misc/musl-cross/`. Those
files can be copied as `config.mak` in musl-cross-make directory. Then,
you just need to run `make` and `make install`.

**NOTE**: when `musl-cross-make` is built, it includes some libtool
archives (`.la`) files. These files contain wrong paths and therefore
create build errors. It is
[recommended](https://github.com/richfelker/musl-cross-make/issues/166)
to delete them:

	$ find <CROSS_DIRECTORY> -type -iname "*.la" -delete

Once the cross compiler has been successfully compiled, you can install
it the `PATH`. **solyste** will scan the `PATH` and also the
following directory to find the binaries:

  - `cross-ARCHITECTURE` in `SOL_WORK_DIR`

I advice you to use this directory. It is much more cleaner as the host
will not be polluted by spare files.

Obviously, `ARCHITECTURE` has to be replaced by the architecture. It
should match the variable `SOL_INSTRU`, as declared in
`scripts/libs/gvars`. Assuming you want to build **solyste** for `rpi`,
a directory `cross-arm/` will be needed in `/build/solyste/`.  
In any case, if you did a mistake and if the binaries were not found,
the scripts should detect it and display a similar message:

	ERR: The following deps are missing: mips64-linux-musl-gcc

**NOTE**: umask `022` is recommended prior to cloning the repository or
before the build.

Using prebuilt toolchains is also possible. I quickly tested toolchains
from [skarnet.org](https://skarnet.org/toolchains/).

## Dependencies to build the system

On **Void Linux**, a meta-package is available and it provides you
everything to build **solyste**:

	$ xbps-install -Sy base-devel

On **Debian**/**Ubuntu**, you can install `build-essential`. It
should be enough:

	$ apt-get update && apt-get install build-essential

On **Arch Linux**, simply install `base-devel` (some useless packages
are included, like `texinfo` or `groff`):

	$ pacman -Sy base-devel

## Supported boards

  * Raspberry Pi 1 A and B+ (arm1176jzf-s) → build arg: `rpi`
  * Raspberry Pi 3 Model B (cortex-a53) → build arg: `rpi3`
  * Turris Omnia → build arg: `omnia`
  * QEMU x86_64 (for emulation) → build arg: `qemu`
  * Dell Wyse 3030 LT → build arg: `wyse`
  * EdgeRouter Lite (ERLite-3) → build arg: `erlite` (read **NOTE**)

**NOTE**: Unfortunately, ERLite-3 board support was removed from
`master` branch, due to awful network performance. For the full report,
read this [article](https://ywstd.fr/blog/2017/erlite-fiasco.html).
You can still switch to `erlite3`
[branch](https://framagit.org/Ypnose/solyste/tree/erlite3) to
test it. This branch is **unmaintained** and lacks many features added
after the removal.

**NOTE**: To help you installing **solyste** on Turris Omnia, read my
dedicated [article](https://ywstd.fr/blog/2020/solyste-omnia.html).

## Build the system

A wrapper called `build.sh` is available. This wrapper will launch
all the scripts with the correct arguments. It is an easy way to create
the entire system.

For example, to build **solyste** for a Raspberry Pi, run:

	$ sh build.sh rpi

The arguments are listed above. The file `build.conf` contains
variables to interact with scripts. To include it, use `-c` option.

	$ sh build.sh -c build.conf rpi

You can modify `build.conf` to tune the full build. Actually, it is the
recommended way to proceed.

There is a `base` group for the "purest" system. Of course, it can be
expanded.

Each script in `scripts/` directory is standalone. It allows
convenient debugging if a problem occurs. Advanced users can also avoid
using `build.sh` and launch the scripts by hand.

	$ sh -x scripts/create-userland rpi base 2>&1 | tee RPI_DEBUG.txt

For some reason, if one script failed, you can simply launch `build.sh`
command once again after you fixed the issue, the build will resume.

When it's over, two directories should be created in `SOL_WORK_DIR` :
`kernel-<BOARD>` and `solfs-<BOARD>`. `kernel-<BOARD>` contains the
kernel and the device tree files. `solfs-<BOARD>` contains the system.

## Network configuration

Network configuration will be written by `scripts/setup-system`. This
script will ask you the required settings. At boot, network setup is
applied by `/etc/rc.network`.

### DHCP

DHCP is not supported. I do not need this protocol.

### Bridge

Adding interfaces to a bridge can be done with a file
`/etc/bridge.<NAME>`. All interfaces written in the file will be added
to the bridge `<NAME>`. If an interface is missing on the system, it
will be ignored.
For example, creating a bridge `br0` with `eth0`, `eth1` & `eth4` can
be achieved with this file :

	$ cat /etc/bridge.br0
	eth0
	eth1
	eth4

Empty lines or lines starting by `#` are ignored.

### VLAN

VLAN on an interface can be activated using a simple file. The file
has to use this naming convention : `/etc/vlan.<INF>.<VLAN_ID>`.
For example, adding `eth0` to VLAN ID `1024` will be done with an empty
file called `vlan.eth0.1024` in `/etc`.

### Static IP

Network settings are stored in `/etc/net.<INF>` files. `<INF>` has to
be replaced by the infertace you need. For example, `eth0` settings
will be defined in `/etc/net.eth0`. Inside the file, the syntax is the
following :

	<IP> <NETMASK> <ROUTE>

`<IP>` & `<NETMASK>` are mandatory. `<ROUTE>` is optional. `<NETMASK>`
can be CIDR notation (e.g., `24`) or full netmask (e.g., `255.255.255.0`).

	$ cat /etc/net.eth0
	192.168.10.10 24 192.168.10.1

Empty lines or lines starting by `#` are ignored.

## Final words

By default, we assume the filesystem is compressed with SquashFS. That's
why some config files have `rootfstype=squashfs` option. Compression is
not automatically done to allow users to set specific configuration in
`solfs-<BOARD>`. Once it is over, you can simply launch :

	$ ./scripts/compress-fs <BOARD>

If you want to stick to other filesystem, it is still possible. Proceed
as you were doing before. This filesystem will be mounted as read-only.
Please keep it mind before using **solyste**. If you used `build.sh`
script, `scripts/setup-system` should ask you if you want read-write
filesystem. If you didn't, make sure `/etc/rc.conf` has
`SOLYSTE_FS_READWRITE=YES` option, before deploying the system.

## Bugs

Despite the care taken in writing POSIX™-friendly scripts, some issues
are likely to happen. `local` builtin is often used but not POSIX™ and
therefore is not implemented in a very few shells.

It was tested with `dash` (the `SHELL` used to develop the code), `bash`
and `ash` (merged in `busybox`). `posh` support is currently UNKNOWN.
Feel free to run it to report back! The shells are not regulary tested,
so strange surprises may happen.

## Can I contribute?

Yes, you can, send me patches. So far, almost nobody was interested by
this project. Maybe because I did/designed it *for* me. Anyway, I only
share what I wrote, to be consistent with my ideals. Being alone, free
of any duties is the most valuable resource. And not having to follow
some obscure and drastic guidelines helps me to concentrate on the
important matters.

## Author

Ypnose - [ywstd.fr](http://ywstd.fr)

## License

BSD 3-Clause License. Check
[LICENSE](https://framagit.org/Ypnose/solyste/blob/master/LICENSE).

## Special thanks

  * I would like to thank [SolidRun](https://www.solid-run.com/)
    for supporting me and sending me top-notch hardware.
  * To my pillow for giving me so much inspiration during the night.

## FAQ

  * Are you human ? Yes.
